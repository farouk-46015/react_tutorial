

// const circle = {
//     radius : 1 ,
//     locations :{
//         x : 1 ,
//         y : 4
//     },
//     draw : function(){
//         console.log('draw');
//     }
// }

//****************************** 
// Build Object by  Factory 
//************************** */

const CreateCircle = (radius,x,y,value)=>{
    return {
        radius,
        locations: {
            x ,
            y
        } ,
        draw: function(){
            console.log('value');
        }
    }
}

let circle = CreateCircle(1 , 11 , 5 , 'draw');
console.log(circle);

//*************************** */
// Build Object by Constructor
//************************ */
// ES6    
// const Circle =(radius) => {
//     console.log('this',this);
//     this.radius = radius ; 
//     this.location = {
//         x : 1, 
//         y : 4
//     };
//     this.draw = ()=>{
//         console.log('draw');
//     }
// }

// const another = new Circle(1);

function Circle(raduis){
    console.log('this', this);
    // this.raduis = raduis ;
    // this.draw = function(){
    //   console.log('draw');
    // }
  }
  
  const anthor = Circle(1);
