import React from 'react';


const Validation = (props)=>{

    let validationMessage= 'text long enough';

    if(props.charcterLength <  props.MinLength ){
        validationMessage = 'Text too short ';
    }

        return <div>{validationMessage}</div>
}

export default Validation;